using System;
using System.Collections.Generic;
using System.Management.Automation;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using advops.Data;
using Microsoft.AspNetCore.SignalR;
using advops.Hubs;
using Newtonsoft.Json.Linq;

namespace advops.Controllers
{

	[Route("api/[controller]")]
	[ApiController]
	public class ScriptController : ControllerBase
	{
		private readonly ApplicationDbContext _context;
		private readonly IHubContext<NotificationHub> _hubContext;
		public ScriptController(ApplicationDbContext context, IHubContext<NotificationHub> hubContext)
		{
			_context = context;
			_hubContext = hubContext;
		}

		[Authorize]
		[HttpPut("{id}/{tagBranch?}")]
		public async Task<ActionResult<string>> ExecuteScript(int id, string tagBranch = "")
		{
			var setting = await _context.Settings.FindAsync(id);
			var disk = System.IO.Directory.GetCurrentDirectory().Substring(0, 2);

			if (setting == null)
			{
				return NotFound();
			}
			await SendMessage("Criando pastas");

			// verifica se tem que criar pasta no disco atual na raiz
			var scriptContents = new StringBuilder();
			scriptContents.AppendLine("If(!(test-path \"\\advops\")){");
			scriptContents.AppendLine("   New-Item -ItemType Directory -Force -Path \"\\advops\"");
			scriptContents.AppendLine("}");

			// executa e recria o script
			await RunScript(scriptContents);
			scriptContents = new StringBuilder();
			scriptContents.AppendLine("cd \\advops");

			// verifica se tem que criar pasta no disco atual na raiz
			scriptContents.AppendLine("If(!(test-path \"project\")){");
			scriptContents.AppendLine("   New-Item -ItemType Directory -Force -Path \"project\"");
			scriptContents.AppendLine("}");
			scriptContents.AppendLine("cd project");

			// executa e recria o script
			await RunScript(scriptContents);
			scriptContents = new StringBuilder();
			scriptContents.AppendLine("cd \\advops\\project");
			await SendMessage("Fazendo clone do projeto ADVPL");

			// verifica se já tem a pasta do git para esse settings
			scriptContents.AppendLine("If(!(test-path \"settings_" + id.ToString() + "\")){");
			scriptContents.AppendLine("   git clone " + setting.GitAdress + " settings_" + id.ToString());
			scriptContents.AppendLine("}");
			scriptContents.AppendLine("cd settings_" + id.ToString());

			// atualiza os dados da branch
			scriptContents.AppendLine("git clean -fd --exclude=.logs/");
			scriptContents.AppendLine("git checkout " + (String.IsNullOrEmpty(setting.Branch) ? tagBranch : setting.Branch));
			scriptContents.AppendLine("git pull");

			// executa e recria o script
			await RunScript(scriptContents);
			scriptContents = new StringBuilder();
			scriptContents.AppendLine("cd \\advops");
			await SendMessage("Fazendo clone do projeto do aplicador ADVPL");

			// verifica se já tem a pasta do git para o aplicador advpl
			scriptContents.AppendLine("If(!(test-path \"advpl_compiler\")){");
			scriptContents.AppendLine("   git clone https://gitlab.com/BSCode/aplicador_protheus_ts.git advpl_compiler");
			scriptContents.AppendLine("}");
			scriptContents.AppendLine("cd advpl_compiler");

			// atualiza os dados da branch
			scriptContents.AppendLine("git clean -fd");
			scriptContents.AppendLine("git checkout master");
			scriptContents.AppendLine("git pull");

			// executa e recria o script
			await RunScript(scriptContents);
			scriptContents = new StringBuilder();
			await SendMessage("Criando configurações de aplicação");

			// cria arquivo de confiruração
			var configtext = System.IO.File.ReadAllText(@"\advops\advpl_compiler\src\config.json");
			JObject config = JObject.Parse(configtext);

			config["environments"][0]["environment"] = setting.Environment;
			config["environments"][0]["server"] = setting.Server;
			config["environments"][0]["port"] = setting.Port.ToString();
			config["environments"][0]["serverVersion"] = setting.Version;
			config["environments"][0]["includeList"] = disk + @"/advops/project/settings_" + id.ToString() + @"/" + setting.IncludePath;
			config["environments"][0]["user"] = setting.User;
			config["environments"][0]["smartClientPath"] = disk + @"/advops/advpl_compiler/smartclient";
			config["environments"][0]["ssl"] = setting.Ssl;
			config["environments"][0]["pass"] = setting.Password;

			config["compilePath"] = disk + @"/advops/project/settings_" + id.ToString() + @"/";
			config["PatchsPath"] = setting.PatchPathApply;
			config["oldPatchsPath"] = setting.PatchPathOldApply;
			config["compileFolderRegex"] = String.IsNullOrEmpty(setting.Regex) ? @".*\.(prw|prx|prg|apw|aph|tres|png|bmp|res|apl|tlpp|4gl|per|msg|ahu|app)" : setting.Regex;

			System.IO.File.WriteAllText(@"\advops\advpl_compiler\src\config.json", config.ToString());

			// executa e recria o script
			scriptContents = new StringBuilder();
			await SendMessage("Copiando RPO BASE");

			// Copia o RPO BASE
			scriptContents.AppendLine("robocopy \"" + setting.ProtheusPath + "\\apo\\rpo_base\" \"" + setting.ProtheusPath + "\\apo\\compilacao\"");

			// executa e recria o script
			await RunScript(scriptContents);
			scriptContents = new StringBuilder();
			await SendMessage("Preparando RPO");

			// Prepara o RPO(compila, aplica patch e desfragmenta)
			scriptContents.AppendLine("cd \\advops\\advpl_compiler");
			scriptContents.AppendLine("npm i");
			scriptContents.AppendLine("npm audit fix");
			scriptContents.AppendLine("npm run build");

			// executa e recria o script
			await RunScript(scriptContents);
			await SendMessage("Finalizado");
			return "";
		}

		private async Task SendMessage(string message)
		{
			Console.WriteLine(message);
			await _hubContext.Clients.All.SendAsync("SendMessage", new { scriptText = message });
		}

		public async Task RunScript(StringBuilder scriptContents)
		{
			//mostra os comandos no console
			scriptContents.AppendLine("Set-PSDebug -Trace 1");

			// create a new hosted PowerShell instance using the default runspace.
			// wrap in a using statement to ensure resources are cleaned up.

			using (PowerShell ps = PowerShell.Create())
			{
				// specify the script code to run.
				ps.AddScript(scriptContents.ToString());

				// execute the script and await the result.
				var pipelineObjects = await ps.InvokeAsync().ConfigureAwait(false);

				// print the resulting pipeline objects to the console.
				foreach (var item in pipelineObjects)
				{
					item.BaseObject.ToString();
					await SendMessage(item.BaseObject.ToString());
				}
			}
		}

	}

}