import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BaseService } from '../base.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SettingsService extends BaseService {

  constructor(
    http: HttpClient,
    @Inject('BASE_URL') baseUrl: string) {
    super(http, baseUrl);
  }

  getData<ApiResult>(
    pageIndex: number,
    pageSize: number,
    sortColumn: string,
    sortOrder: string,
    filterColumn: string,
    filterQuery: string
  ): Observable<ApiResult> {
    var url = this.baseUrl + 'api/Settings';
    var params = new HttpParams()
      .set("pageIndex", pageIndex.toString())
      .set("pageSize", pageSize.toString())
      .set("sortColumn", sortColumn)
      .set("sortOrder", sortOrder);

    if (filterQuery) {
      params = params
        .set("filterColumn", filterColumn)
        .set("filterQuery", filterQuery);
    }

    return this.http.get<ApiResult>(url, { params });
  }

  get<Setting>(id): Observable<Setting> {
    var url = this.baseUrl + "api/Settings/" + id;
    return this.http.get<Setting>(url);
  }

  put<Setting>(item): Observable<Setting> {
    var url = this.baseUrl + "api/Settings/" + item.id;
    return this.http.put<Setting>(url, item);
  }

  post<Setting>(item): Observable<Setting> {
    var url = this.baseUrl + "api/Settings";
    return this.http.post<Setting>(url, item);
  }
}
