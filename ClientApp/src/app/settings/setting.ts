export interface Setting {
	id: number;
	name: string;
	environment: string;
	protheusPath: string;
	server: string;
	port: number;
	user: string;
	password: string;
	gitAdress: string;
	branch: string;
	gitUser: string;
	gitPassword: string;
	lastApply: Date;
	includePath: string;
	version: string;
	ssl: Boolean;
	regex: string;
	patchPathOldApply: string;
	patchPathApply: string;
	createdDate: Date;
	lastModifiedDate: Date;
	running: boolean;
}
