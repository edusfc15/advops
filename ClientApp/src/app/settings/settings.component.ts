import { Component, OnInit, OnDestroy } from '@angular/core';
import { Setting } from './setting';
import { SettingsService } from './settings.service';
import { ApiResult } from '../base.service';
import { LazyLoadEvent } from 'primeng/api';
import { Router } from '@angular/router';
import { ScriptService } from './script.service';
import { Subscription } from 'rxjs';
import { Message } from './message';
import { SignalRService } from './signalr-service';
import { TerminalService } from 'primeng/terminal';


@Component({
	selector: 'app-settings',
	templateUrl: './settings.component.html',
	styleUrls: ['./settings.component.css']

})
export class SettingsComponent implements OnInit, OnDestroy {

	private signalRSubscription: Subscription;
	public content: Message;


	settings: Setting[];

	defaultPageIndex: number = 0;
	defaultPageSize: number = 5;
	defaultSortColumn: string = "id";
	defaultSortOrder: string = "asc";
	defaultFilterColumn: string = "name";
	totalRecords: number;
	scriptText: string = '';

	cols: any[];
	sort: any;

	filterQuery: string = null;


	loading: boolean;

	constructor(
		private settingService: SettingsService,
		private scriptService: ScriptService,
		private router: Router,
		private signalrService: SignalRService,
	) {
		this.signalRSubscription = this.signalrService.getMessage().subscribe(
			(message) => {
				if (message.scriptText) {
					this.scriptText += message.scriptText + '\n';
				}
			});

	}

	ngOnInit() {

		this.cols = [
			{ field: 'id', header: 'Id' },
			{ field: 'name', header: 'Nome' },
			{ field: 'lastApply', header: 'Ultima aplicação' },
		];
	}

	updateFilterQuery(query: string = null, field: string) {

		this.defaultFilterColumn = field;
		this.filterQuery = query;
		this.loadData({});

	}

	loadData(event: LazyLoadEvent) {

		var sortOrderCommand = event.sortOrder == 1 ? 'asc' : 'desc';

		var sortColumn = (event.sortField)
			? event.sortField
			: this.defaultSortColumn;

		var sortOrder = (event.sortOrder)
			? sortOrderCommand
			: this.defaultSortOrder;

		var filterColumn = (this.filterQuery)
			? this.defaultFilterColumn
			: null;

		var filterQuery = (this.filterQuery)
			? this.filterQuery
			: null;

		var pageIndex = (event.first)
			? event.first / event.rows : this.defaultPageIndex;

		var pageSize = (event.rows)
			? event.rows : this.defaultPageSize

		this.settingService.getData<ApiResult<Setting>>(
			pageIndex,
			pageSize,
			sortColumn,
			sortOrder,
			filterColumn,
			filterQuery)
			.subscribe(result => {
				this.totalRecords = result.totalCount;
				this.settings = result.data;
			}, error => console.error(error));

	}

	onEdit(event) {
		this.router.navigate(['/configuracao', event.data.id])
	}

	RunApplication(setting: Setting) {
		setting.running = true;
		// EDIT mode
		this.scriptService.put(setting.id).subscribe(
			(result) => {
				console.log(`Sucesso ${result}`)
				setting.running = false;
			},
			(error) => {
				console.log(error)
				setting.running = false;
			}
		);
	}

	ngOnDestroy(): void {
		this.signalrService.disconnect();
		this.signalRSubscription.unsubscribe();
	}

}
