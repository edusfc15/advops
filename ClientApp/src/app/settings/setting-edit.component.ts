import { Component, Inject } from "@angular/core";
// import { HttpClient, HttpParams } from '@angular/common/http';
import { ActivatedRoute, Router } from "@angular/router";
import {
	FormGroup,
	FormControl,
	Validators,
	AbstractControl,
	AsyncValidatorFn,
} from "@angular/forms";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { BaseFormComponent } from "../base.form.component";
import { ApiResult } from "../base.service";
import { SettingsService } from "./settings.service";
import { Setting } from "./setting";

@Component({
	selector: "app-setting-edit",
	templateUrl: "./setting-edit.component.html",
	styleUrls: ["./setting-edit.component.css"],
})
export class SettingEditComponent extends BaseFormComponent {
	title: string;
	form: FormGroup;
	setting: Setting;

	id?: number;

	constructor(
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private settingService: SettingsService
	) {
		super();
	}

	ngOnInit() {
		this.form = new FormGroup(
			{
				name: new FormControl("", Validators.required),
				environment: new FormControl("", Validators.required),
				server: new FormControl("", Validators.required),
				protheusPath: new FormControl(""),
				port: new FormControl(0, Validators.required),
				user: new FormControl("", Validators.required),
				password: new FormControl("", Validators.required),
				gitAdress: new FormControl("", Validators.required),
				branch: new FormControl(""),
				gitUser: new FormControl("", Validators.required),
				gitPassword: new FormControl("", Validators.required),
				includePath: new FormControl("", Validators.required),
				version: new FormControl("", Validators.required),
				ssl: new FormControl(false),
				regex: new FormControl("", Validators.required),
				patchPathOldApply: new FormControl(""),
				patchPathApply: new FormControl(""),
			},
			null,
			null
		);

		this.loadData();
	}

	loadData() {
		this.id = +this.activatedRoute.snapshot.paramMap.get("id");
		if (this.id) {
			// EDIT MODE
			this.settingService.get<Setting>(this.id).subscribe(
				(result) => {
					this.setting = result;
					this.title = "Alterar - " + this.setting.name;

					this.form.patchValue(this.setting);
				},
				(error) => console.error(error)
			);
		} else {
			// ADD NEW MODE

			this.title = "Inclusão de configuração...";
			this.setting = <Setting>{};
		}
	}

	onSubmit() {
		for (let i in this.form.value) {
			if (this.form.value.hasOwnProperty(i)) {
				this.setting[i] = this.form.value[i];
			}
		}

		if (this.id) {
			// EDIT mode
			this.settingService.put<Setting>(this.setting).subscribe(
				() => {
					this.router.navigate(["/configuracoes"]);
				},
				(error) => console.log(error)
			);
		} else {
			// ADD NEW mode
			this.settingService.post<Setting>(this.setting).subscribe(
				() => {
					this.router.navigate(["/configuracoes"]);
				},
				(error) => console.log(error)
			);
		}
	}
}
