import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BaseService } from '../base.service';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class ScriptService extends BaseService {

	constructor(
		http: HttpClient,
		@Inject('BASE_URL') baseUrl: string) {
		super(http, baseUrl);
	}

	get(id): Observable<any> {
		var url = this.baseUrl + "api/Script/" + id;
		return this.http.get<any>(url);
	}

	put(id, branch = ''): Observable<any> {
		var url = this.baseUrl + "api/Script/" + id + "/" + branch;
		return this.http.put<any>(url, {});
	}

	post(item): Observable<any> {
		var url = this.baseUrl + "api/Script";
		return this.http.post<any>(url, item);
	}

	getData<ApiResult>(
		pageIndex: number,
		pageSize: number,
		sortColumn: string,
		sortOrder: string,
		filterColumn: string,
		filterQuery: string
	): Observable<ApiResult> {
		var url = this.baseUrl + 'api/Script';
		var params = new HttpParams()
			.set("pageIndex", pageIndex.toString())
			.set("pageSize", pageSize.toString())
			.set("sortColumn", sortColumn)
			.set("sortOrder", sortOrder);

		if (filterQuery) {
			params = params
				.set("filterColumn", filterColumn)
				.set("filterQuery", filterQuery);
		}

		return this.http.get<ApiResult>(url, { params });
	}

}
