using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;


namespace advops.Models
{
	public class Setting
	{

		public Setting()
		{
		}

		[Key]
		public int Id { get; set; }

		[Required]
		public string Name { get; set; }
		[Required]
		public string Environment { get; set; }
		[Required]
		public string Server { get; set; }
		[Required]
		public int Port { get; set; }
		[Required]
		public string User { get; set; }
		public string Password { get; set; }
		[Required]
		public string GitAdress { get; set; }
		[Required]
		public string GitUser { get; set; }
		[Required]
		public string GitPassword { get; set; }
		public string Branch { get; set; }
		[Required]
		public DateTime LastApply { get; set; }
		[Required]
		public string IncludePath { get; set; }
		[Required]
		public string Version { get; set; }
		public Boolean Ssl { get; set; }
		public string Regex { get; set; }
		public string PatchPathOldApply { get; set; }
		public string PatchPathApply { get; set; }
		[Required]
		public string ProtheusPath { get; set; }
		public DateTime CreatedDate { get; set; }
		public DateTime LastModifiedDate { get; set; }
	}
}